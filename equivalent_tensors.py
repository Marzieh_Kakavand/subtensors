from itertools import permutations, product
import copy
#M = [[[0, 1, 2], [3, 4, 5], [6, 7, 8]], [[9, 10, 11], [12, 13, 14], [15, 16, 17]],[[18, 19, 20], [21, 22, 23], [24, 25, 26]]]


def equivalent_tensors(M):
    a, b, c = [len(M[0][0]), len(M[0]), len(M)]
    A = [{s: p for s, p in enumerate(x)} for x in list(permutations(range(a), a))]
    B = [{s: p for s, p in enumerate(x)} for x in list(permutations(range(b), b))]
    C = [{s: p for s, p in enumerate(x)} for x in list(permutations(range(c), c))]
    M_equivalent_list = []
    for x, y, z in list(product(*[A, B, C])):
        M_equivalent = copy.deepcopy(M)
        for i in x.keys():
            for j in y.keys():
                for k in z.keys():
                    M_equivalent[i][j][k] = M[x[i]][y[j]][z[k]]
        M_equivalent_list.append(M_equivalent)
    return map(eval,set(map(str, M_equivalent_list)))


def generate_tensor(t, dim):

    tensor = [[[0 for i in range(dim[0])]for j in range(dim[1])] for k in range(dim[2])]
    for x in t:
        tensor[x[0]][x[1]][x[2]] = 1
    return tensor


def generate_t(tensor):
    t = ()
    a, b, c = [len(tensor[0][0]), len(tensor[0]), len(tensor)]
    for i in range(a):
        for j in range(b):
            for k in range(c):
                if tensor[i][j][k] == 1:
                    t = t + ((i, j, k),)
    return t


print map(generate_t, equivalent_tensors(generate_tensor(((0, 0, 0), (0, 0, 1), (0, 0, 2), (0, 1, 1)), [3, 3, 3])))

lis = list(permutations(product(*[range(3), range(3), range(3)]), 4))

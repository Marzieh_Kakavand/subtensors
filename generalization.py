
from itertools import product, izip_longest, chain, starmap, permutations
import random
import sympy
import copy
import subprocess
import subtensors222
from straighten_GL import straighten3h


def tableau_transpose(tab):
    import operator
    res = all(starmap(operator.ge,zip(tab, tab[1:]))) & all(x >= 0 for x in tab)
    if not res:
        print 'The input should be a sequence of nonincreasing positive integers.'
    else:
        tab_range = map(lambda x: range(x), tab)
        trans_tab=[reduce(lambda x, y: x + y, tab_range).count(x) for x in range(tab[0])]
        return trans_tab


def kron_coefficient(tab):
    kronecker_coefficient = subprocess.check_output(["/Applications/SageMath/sage", "kron.sage", str(tab)])
    return eval(kronecker_coefficient)

# tableau, transposed tableau,  kronecker coefficient,
# [4,2,0], [2,2,1,1],   2
# [3,2,1], [3,2,1],     5
# [5,2,0], [2,2,1,1,1], 2
# [4,2,1], [3,2,1,1],   9
# [3,2,2], [3,3,1],     2
# [5,2,1], [3,2,1,1,1], 9
# [4,3,1], [3,2,2,1],   8
# [4,2,2], [3,3,1,1],   6
# rank should be greater than numbers in transposed tableau
# for the tableau [4,3,2,0] we have four [1,0,0,0], three [0,1,0,0] and two [0,0,1,0]
# then we have the transposed tableau that is [3,3,2,1] so we have
# [[1,0,0,0],[0,1,0,0],[0,0,1,0]],
# [[1,0,0,0],[0,1,0,0],[0,0,1,0]],
# [[1,0,0,0],[0,1,0, 0]]
# [[1,0,0,0]]

# [3,2,1], [3,2,1],  5
rank = 3
tableau = [3,2,1]
print 'tableau_transpose'
tableau_transpos = tableau_transpose(tableau)
print tableau_transpos
d = sum(tableau)
kron_coeff = kron_coefficient(tableau)
tensor_shape = [2, 2, 2]
target_dim = [tensor_shape[p]*tensor_shape[(p+1) % len(tensor_shape)] for p in range(len(tensor_shape))]
#target_dim=[i*j, j*k, k*i]
target_tensor = subtensors222.tensor_exhibition(211212221222)
matrix_mul_identifier = [[tensor_shape[1]*i+j, tensor_shape[2]*j+k, tensor_shape[0]*k+i] for k in range(tensor_shape[2]) for j in range(tensor_shape[1]) for i in range(tensor_shape[0])]
unit_matrix = [[[int(i == j) for i in range(s)] for j in range(s)] for s in target_dim]


def grouper(iterable, l, fill_value = None):
    a = l[0]
    c = a
    arg = [iter(iterable[:a])] * a
    it_0 = izip_longest(fillvalue = fill_value, *arg)
    for i in l[1:]:
        b = c
        c = i+b
        args = [iter(iterable[b:c])] * a
        it_1 = izip_longest(fillvalue = fill_value, *args)
        it_0 = chain(it_0,it_1)
    return it_0


def has_zero_pattern(T1,T2,T3):
    for c1, c2, c3 in product(T1, T2, T3):
        if len(set(c1).intersection(c2).intersection(c3)) >= 2:
            return True
    return False


def random_perms(T):
    while True:
        T1 = list(grouper(range(d), T, 'EmptyA'))
        L = range(d)
        random.shuffle(L) # in-place
        L2 = copy.deepcopy(L)
        T2 = list(grouper(L2, T, 'EmptyB'))
        random.shuffle(L) # in-place
        L3 = copy.deepcopy(L)
        T3 = list(grouper(L3, T, 'EmptyC'))
        if not has_zero_pattern(T1, T2, T3):
            return (L2, L3)


def matrix_rank(a):
    m_rank = subprocess.check_output(["python", "rank.py", str(a)])
    return int(m_rank)


def left_kernel_basis(a):
    kernel = subprocess.check_output(["/Applications/SageMath/sage", "left_kernel_basis.sage", str(a)])
    return eval(kernel)


def index_to_evaluate(r, tr_tab):
    perm_list = [list(permutations(range(r), i)) for i in tr_tab]
    return product(*perm_list)


def matrix_det(l):
    n = len(l)
    if n > 2:
        i = 1
        t = 0
        sum = 0
        while t <= n - 1:
            d = {}
            t1 = 1
            while t1 <= n - 1:
                m = 0
                d[t1] = []
                while m <= n - 1:
                    if m == t:
                        u = 0
                    else:
                        d[t1].append(l[t1][m])
                    m += 1
                t1 += 1
            l1 = [d[x] for x in d]
            sum = sum + i * (l[0][t]) * (matrix_det(l1))
            i = i * (-1)
            t += 1
        return sum
    else:
        if n == 2:
            return l[0][0] * l[1][1] - l[0][1] * l[1][0]
        else:
            return l[0][0]

def det(tensor, list, p):
    a = len(list)
    return matrix_det([tensor[i][p][0:a] for i in list])


def evaluator(tau1, tau2, tensor, index):
    r = len(tensor)
    output = 0
    f = 0
    for element in index:
        f += 1
        summ = 1
        iter_list = list(chain(*element))
        initial_value = 0
        for nu in tableau_transpos:
            summ = summ * det(tensor,[iter_list[i] for i in range(initial_value, initial_value+nu)], 0)
            summ = summ * det(tensor, [iter_list[tau1[i]] for i in range(initial_value, initial_value+nu)], 1)
            summ = summ * det(tensor, [iter_list[tau2[i]] for i in range(initial_value, initial_value+nu)], 2)
            initial_value = initial_value + nu
        output = output + summ

    return output


def construct_basis():
    max_tries = 30  # 30
    matrix = []
    ideal_rank = 0
    basis = []
    tensor_length = reduce(lambda a, b: a * b, tensor_shape)
    l_0 = tensor_length/ 2
    l_1 = tensor_length - l_0
    perturbations = [[[unit_matrix[j][m[j]] for j in range(len(tensor_shape))] for m in matrix_mul_identifier[0:l_0]] +
                     [[unit_matrix[0][matrix_mul_identifier[j][0]]]+[[random.randint(-5,5) for p in range(s)] for i, s in enumerate(target_dim[1:])] for j in range(l_1)] for w in range(kron_coeff)]
    #  [[[1, 0, 0, 0], [1, 0, 0, 0],    [1, 0, 0, 0]],    [[0, 0, 1, 0], [1, 0, 0, 0], [0, 1, 0, 0]],   [[0, 1, 0, 0], [0, 0, 1, 0], [1, 0, 0, 0]],   [[0, 0, 0, 1], [0, 0, 1, 0], [0, 1, 0, 0]],
    #   [[1, 0, 0, 0], [-1, -2, -4, 2], [-5, 4, -2, -2]], [[0, 0, 1, 0], [-3, 3, 2, 3], [4, -5, 0, 5]], [[0, 1, 0, 0], [2, 4, 1, -4], [5, 0, 3, -2]], [[0, 0, 0, 1], [5, -5, -4, 5], [-4, -5, 3, 0]]]
    # by definition of the every element in perturbations, since in modulo 4 the first element are same,
    # in each index tuple we should not have numbers that are equal modulo 4.
    # we need to produce tuple indexes that does not contain numbers that modulo four are equal
    # here since we have a tensor with length 8, if we choose a tuple of number between zero and three,
    # and each time we add four to any entrie the diffrence between these number will not be four.
    # this not depond on length of tuples. since we add foure to every number each time it can cover all number between zero and seven(or one and eight).
    # we defined this index corresponding to this perturbations
    #print perturbations
    perm = [random_perms(tableau) for i in range(max_tries)]
    index = list(product(*[list(chain(*[list(product(*[[j[i],j[i]+4] for i in range(w)]))for j in permutations(range(4), w)])) for w in [3,2,1,0]]))
    for tau1 , tau2 in perm:
        new_matrix = copy.deepcopy(matrix)
        row_completed = []
        for perturbation in perturbations:
            entry = evaluator(tau1, tau2, perturbation, index)
            row_completed.append(entry)
        new_matrix.append(row_completed)
        new_rank = matrix_rank(new_matrix)
        if new_rank > ideal_rank:
            matrix = new_matrix
            ideal_rank = new_rank
            basis.append([tau1,tau2])
        if ideal_rank == kron_coeff:
            print 'basis_matrix'
            print new_matrix
            print 'basis_rank'
            print ideal_rank
            break
    return basis


def construct_ideal_basis(basis):
    tensors = []
    for k in range(kron_coeff):
        tensor = [[[random.randint(-5,5) for j in range(s)] for s in target_dim] for s in range(rank)]
        tensors.append(tensor)
    index = list(product(*[list(permutations(range(rank), w)) for w in tableau_transpos]))
    matrix = []
    for tau1, tau2 in basis:
        row_completed = []
        for tensor in tensors:
            entry = evaluator(tau1, tau2, tensor, index)
            row_completed.append(entry)
        matrix.append(row_completed)

    kernel_basis = left_kernel_basis(matrix)
    return kernel_basis


def evaluate_straighten(tau1, tau2):
    output = {}
    for element in index_to_evaluate(rank, tableau_transpos):
        iter_list = list(chain(*element))
        initial_value = 0
        tab0 = []
        tab1 = []
        tab2 = []
        for nu in tableau_transpos:
            tab0.append([iter_list[i] for i in range(initial_value, initial_value + nu)])
            tab1.append([iter_list[tau1[i]] for i in range(initial_value, initial_value + nu)])
            tab2.append([iter_list[tau2[i]] for i in range(initial_value, initial_value + nu)])
            initial_value = initial_value + nu
        st = straighten3h(tab0,tab1,tab2)

        for k,v in st.iteritems():
            if v == 0:
                continue
            else:
                k_hash = int(''.join(map(str,sum(k[0] + k[1] + k[2], ()))))
                if k_hash in output:
                    w = output[k_hash] + v
                    if w == 0:
                        del output[k_hash]
                    else:
                        output[k_hash] = w
                else:
                    output[k_hash] = v
    return output


def verify_ideal_basis(basis, coeffs_list):
    outputs_list = []
    j = 0
    for tau1, tau2 in basis:
        outputs = evaluate_straighten(tau1, tau2)
        outputs_list.append(outputs)
        j += 1
    basis_evals = []
    for i, outputs in enumerate(outputs_list):
        entry = {}
        for k, v in outputs.iteritems():
            if k in entry:
                x = entry[k] + v
                if x == 0:
                    del entry[k]
                else:
                    entry[k] = x
            else:
                entry[k] = v
        basis_evals.append(entry)

    for coeffs in coeffs_list:
        for i in range(len(basis_evals)):
            for k, v in basis_evals[i].iteritems():
                running_sum = coeffs[i] * v
                for j in range(i + 1, len(basis_evals)):
                    if k in basis_evals[j]:
                        running_sum += coeffs[j] * basis_evals[j][k]
                        del basis_evals[j][k]
                if not running_sum == 0:
                    print "verification failed"
                    return False
    return True


def verify_ideal_basis_symbolically(basis, coeffs_list):
    generic_tensor = [[[sympy.var("a%d%d%d" % (i,j,k)) for i in range(p)] for j, p in enumerate(target_dim)] for k in range(rank - 1)] \
                     + [[unit_matrix[s][p-1] for s, p in enumerate(target_dim)]]
    print'generic_tensor'
    print generic_tensor
    index = list(product(*[list(permutations(range(rank), w)) for w in tableau_transpos]))
    outputs_list = []
    j = 0
    for tau1,tau2 in basis:
        outputs = evaluator(tau1, tau2, generic_tensor,index)
        outputs_list.append(outputs)
        j += 1

    basis_evals = []
    for outputs in outputs_list:
        basis_evals.append(outputs)

    for coeffs in coeffs_list:
        lin_comb = 0
        for i in range(len(basis_evals)):
            lin_comb += coeffs[i] * basis_evals[i]
        if not sympy.expand(lin_comb) == 0:
            return False
    return True


def evaluate_target_tensor(basis, coeffs_list):
    index = list(product(*[list(permutations(range(len(target_tensor)), w)) for w in tableau_transpos]))
    outputs_list = []
    j = 0
    print 'target tensor'
    print target_tensor
    for tau1,tau2 in basis:
        outputs = evaluator(tau1, tau2, target_tensor, index)
        outputs_list.append(outputs)
        j += 1

    basis_evals = []
    for outputs in outputs_list:
        basis_evals.append(outputs)

    for coeffs in coeffs_list:
        lin_comb = 0
        for i in range(len(basis_evals)):
            lin_comb += coeffs[i] * basis_evals[i]
        exp_lin_comb = sympy.expand(lin_comb)
        if not sympy.expand(lin_comb) == 0:
            print "verification failed"
            return False

    return True


def main():

    basis = construct_basis()

    coeffs_list = construct_ideal_basis(basis)

    verify_ideal_basis(basis, coeffs_list)

    verify_ideal_basis_symbolically(basis, coeffs_list)

    evaluate_target_tensor(basis, coeffs_list)

main()



#[3, 2, 2]
# kji  ij jk ki  abc
# 000  00 00 00  000
# 001  10 00 01  201
# 002  20 00 02  402
# 010  01 10 00  120
# 011  11 10 01  321
# 012  21 10 02  522
# 100  00 01 10  012
# 101  10 01 11  213
# 102  20 01 12  415
# 110  01 11 10  132
# 111  11 11 11  333
# 112  21 11 12  535
#def evaluate_job(evaluator):
#    return evaluator.evaluate()
#def evaluate_straighten_job(evaluator):
#    return evaluator.evaluate_straighten()
#rand = [[random.randint(-5,5) for j in range(s)] for s in target_dim]

#def index_to_evaluate():
    #tr_tab.reverse()
    #a= len(tr_tab)
    #pr = []
 #   from itertools import product, permutations, chain
 #   A33=[list(product([j[0], j[0] + 5], [j[1], j[1] + 5], [j[2], j[2] + 5])) for j in permutations(range(3), 3)]
  #  AS33=list(chain.from_iterable(A33))
  #  A43 = [list(product([j[0], j[0] + 4], [j[1], j[1] + 4], [j[2], j[2] + 4])) for j in permutations(range(4), 3)]
  #  AS43 = list(chain.from_iterable(A43))
  #  AS=list(set(AS33)-set(AS43))
  #  LASP=[AS for l in range(4)]
   # return product(*LASP)
import sys
from sage.all import *


def kernel_basis(A):
    M = matrix(ZZ, A)
    return map(list,M.left_kernel().basis())

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print ("Usage: %s <A>"%sys.argv[0])
        print ("Outputs a basis for the left kernel of in integer matrix A.")
        sys.exit(1)
    else:
        print kernel_basis(eval(sys.argv[1]))

from itertools import product, izip_longest, chain, starmap, permutations
import random
import sympy
import copy
import subprocess
from straighten_GL import straighten3h


def tableau_transpose(tab):
    import operator
    res = all(starmap(operator.ge,zip(tab, tab[1:]))) & all(x >= 0 for x in tab)
    if not res:
        print 'The input should be a sequence of nonincreasing positive integers.'
    else:
        tab_range = map(lambda x: range(x), tab)
        trans_tab=[reduce(lambda x, y: x + y, tab_range).count(x) for x in range(tab[0])]
        return trans_tab


def grouper(iterable, l, fill_value = None):
    a = l[0]
    c = a
    arg = [iter(iterable[:a])] * a
    it_0 = izip_longest(fillvalue = fill_value, *arg)
    for i in l[1:]:
        b = c
        c = i+b
        args = [iter(iterable[b:c])] * a
        it_1 = izip_longest(fillvalue = fill_value, *args)
        it_0 = chain(it_0,it_1)
    return it_0


def has_zero_pattern(t_1, t_2, t_3):
    for c1, c2, c3 in product(t_1, t_2, t_3):
        if len(set(c1).intersection(c2).intersection(c3)) >= 3:
            return True
    return False


def random_perms(T):
    d = sum(T)
    while True:
        T1 = list(grouper(range(d), T, 'EmptyA'))
        L = range(d)
        random.shuffle(L)
        L2 = copy.deepcopy(L)
        T2 = list(grouper(L2, T, 'EmptyB'))
        random.shuffle(L)
        L3 = copy.deepcopy(L)
        T3 = list(grouper(L3, T, 'EmptyC'))
        if not has_zero_pattern(T1, T2, T3):
            return (L2, L3)


def matrix_rank(a):
    m_rank = subprocess.check_output(["python", "rank.py", str(a)])
    return int(m_rank)


def left_kernel_basis(a):
    kernel = subprocess.check_output(["/Applications/SageMath/sage", "left_kernel_basis.sage", str(a)])
    return eval(kernel)


def index_to_evaluate(r, tr_tab):
    perm_list = [list(permutations(range(r), i)) for i in tr_tab]
    return product(*perm_list)


def matrix_det(l):
    n = len(l)
    if n > 2:
        i = 1
        t = 0
        sum = 0
        while t <= n - 1:
            d = {}
            t1 = 1
            while t1 <= n - 1:
                m = 0
                d[t1] = []
                while m <= n - 1:
                    if m == t:
                        u = 0
                    else:
                        d[t1].append(l[t1][m])
                    m += 1
                t1 += 1
            l1 = [d[x] for x in d]
            sum = sum + i * (l[0][t]) * (matrix_det(l1))
            i = i * (-1)
            t += 1
        return sum
    else:
        if n == 2:
            return l[0][0] * l[1][1] - l[0][1] * l[1][0]
        else:
            return l[0][0]

def mode_det(mode, tensor_components, indices):
    return matrix_det([tensor_components[i][mode][0:len(indices)] for i in indices])


def evaluator(tau1, tau2, tensor_components, indexes, nu):
    s = 0
    for index in indexes:
        p = 1
        j = list(chain(*index))
        k = 0
        for n in nu:
            p = p * mode_det(0, tensor_components, [j[i]       for i in range(k, k + n)])
            p = p * mode_det(1, tensor_components, [j[tau1[i]] for i in range(k, k + n)])
            p = p * mode_det(2, tensor_components, [j[tau2[i]] for i in range(k, k + n)])
            k = k + n
        s = s + p
    return s


def construct_basis(r, n, k_mu, mu, nu):
    tensors = [[[[random.randint(-5,5) for j in range(i)] for i in [n, n, n]] for s in range(r+1)] for w in range(k_mu)]
    index = list(product(*[list(permutations(range(r+1), w)) for w in nu]))
    max_tries = 30
    matrix = []
    basis_rank = 0
    basis = []
    perm = [random_perms(mu) for i in range(max_tries)]
    for tau1, tau2 in perm:
        new_matrix = copy.deepcopy(matrix)
        row_completed = []
        for tensor in tensors:
            entry = evaluator(tau1, tau2, tensor, index, nu)
            row_completed.append(entry)
        new_matrix.append(row_completed)
        new_rank = matrix_rank(new_matrix)
        if new_rank > basis_rank:
            matrix = new_matrix
            basis_rank = new_rank
            basis.append([tau1,tau2])
        if basis_rank == k_mu:
            break
    return basis


def left_kernel_coeffs(basis, r, n,  k_mu, nu):
    tensors = [[[[random.randint(-5, 5) for j in range(i)] for i in [n, n, n]] for s in range(r)] for w in range(k_mu)]
    index = list(product(*[list(permutations(range(r), w)) for w in nu]))
    matrix = []
    for b1, b2 in basis:
        row_completed = []
        for tensor in tensors:
            entry = evaluator(b1, b2, tensor, index, nu)
            row_completed.append(entry)
        matrix.append(row_completed)
    kernel_basis = left_kernel_basis(matrix)
    return kernel_basis


def evaluate_straighten(nu, tau1, tau2,  indexes):
    output = {}
    for index in indexes:
        j = list(chain(*index))
        k = 0
        tab = [[],]*3
        for n in nu:
            tab[0].append([j[i]       for i in range(k, k + n)])
            tab[1].append([j[tau1[i]] for i in range(k, k + n)])
            tab[2].append([j[tau2[i]] for i in range(k, k + n)])
            k = k + n
        st = straighten3h(tab[0], tab[1],tab[2])
        for k,v in st.iteritems():
            if v == 0:
                continue
            else:
                k_hash = int(''.join(map(str,sum(k[0] + k[1] + k[2], ()))))
                if k_hash in output:
                    w = output[k_hash] + v
                    if w == 0:
                        del output[k_hash]
                    else:
                        output[k_hash] = w
                else:
                    output[k_hash] = v
    return output


def verify_coeffs_basis(r, nu, basis, coeffs_list):
    indexes = product(*[list(permutations(range(r), i)) for i in nu])
    outputs_list = []
    j = 0
    for b1, b2 in basis:
        outputs = evaluate_straighten(nu, b1, b2, indexes)
        outputs_list.append(outputs)
        j += 1
    basis_evals = []
    for i, outputs in enumerate(outputs_list):
        entry = {}
        for k, v in outputs.iteritems():
            if k in entry:
                x = entry[k] + v
                if x == 0:
                    del entry[k]
                else:
                    entry[k] = x
            else:
                entry[k] = v
        basis_evals.append(entry)

    for coeffs in coeffs_list:
        for i in range(len(basis_evals)):
            for k, v in basis_evals[i].iteritems():
                running_sum = coeffs[i] * v
                for j in range(i + 1, len(basis_evals)):
                    if k in basis_evals[j]:
                        running_sum += coeffs[j] * basis_evals[j][k]
                        del basis_evals[j][k]
                if not running_sum == 0:
                    print "verification failed"
                    return False
    return True


def verify_ideal_basis_symbolically(basis, coeffs_list):
    generic_tensor = [[[sympy.var("a%d%d%d" % (i,j,k)) for i in range(p)] for j, p in enumerate(t_com)] for k in range(r - 1)] \
                     + [[id[s][p-1] for s, p in enumerate(t_com)]]
    print'generic_tensor'
    print generic_tensor
    index = list(product(*[list(permutations(range(r), w)) for w in nu]))
    outputs_list = []
    j = 0
    for tau1,tau2 in basis:
        outputs = evaluator(tau1, tau2, generic_tensor,index, nu)
        outputs_list.append(outputs)
        j += 1

    basis_evals = []
    for outputs in outputs_list:
        basis_evals.append(outputs)

    for coeffs in coeffs_list:
        lin_comb = 0
        for i in range(len(basis_evals)):
            lin_comb += coeffs[i] * basis_evals[i]
        if not sympy.expand(lin_comb) == 0:
            return False
    return True


def evaluate_sub_com(basis, coeffs_list):
    index = list(product(*[list(permutations(range(len(sub_com)), w)) for w in nu]))
    outputs_list = []
    j = 0

    for tau1,tau2 in basis:
        outputs = evaluator(tau1, tau2, sub_com, index, nu)
        outputs_list.append(outputs)
        j += 1

    basis_evals = []
    for outputs in outputs_list:
        basis_evals.append(outputs)

    for coeffs in coeffs_list:
        lin_comb = 0
        for i in range(len(basis_evals)):
            lin_comb += coeffs[i] * basis_evals[i]
        exp_lin_comb = sympy.expand(lin_comb)
        if not sympy.expand(lin_comb) == 0:
            print "verification failed"
            return False
    return True

def tableau(n):
    k = subprocess.check_output(["/Applications/SageMath/sage", "new_kron.sage", str(n)])
    return eval(k)


def main():
    tensor = input("Enter the tensor: ")
    r = input("What is your estimation for the border rank of this tensor? ")
    n = len(tensor[0][0])
    [k_mu, mu] = tableau(n)
    print k_mu
    print mu
    nu = tableau_transpose(mu)
    basis = construct_basis(r, n, k_mu, mu, nu)
    print basis
    coeffs_list = left_kernel_coeffs(basis, r, n, k_mu, nu)
    print coeffs_list
    print verify_coeffs_basis(r, nu, basis, coeffs_list)
    #verify_ideal_basis_symbolically(basis, coeffs_list)
    #evaluate_sub_com(basis, coeffs_list)
main()
# tableau, transposed tableau,  kronecker coefficient,
# [4,2],   [2,2,1,1],   2
# [5,2],   [2,2,1,1,1], 2
# [3,2,1], [3,2,1],     5
# [4,2,1], [3,2,1,1],   9
# [3,2,2], [3,3,1],     2
# [5,2,1], [3,2,1,1,1], 9
# [4,3,1], [3,2,2,1],   8
# [4,2,2], [3,3,1,1],   6
# rank should be greater than numbers in transposed tableau
# for the tableau [4,3,2] we have four [1,0,0,0], three [0,1,0,0] and two [0,0,1,0]
# then we have the transposed tableau that is [3,3,2,1] so we have
# [[1,0,0,0],[0,1,0,0],[0,0,1,0]],
# [[1,0,0,0],[0,1,0,0],[0,0,1,0]],
# [[1,0,0,0],[0,1,0, 0]]
# [[1,0,0,0]]

# r should be at least 2.
# when n=2 we could consider the [4, 2]([5, 2], [6, 2]) tableau that it's kronecker coefficient is 2.
# since every entry of its transpose [2,2,1,1] is not greater than r this is  adequate to n and r.
# then for every n, r we should appropriate tableau.
# when r is 2 the list of indexes for providing basis is not too large
# It would be product(*[permutations(range(2), i) for i in [2,2,1,1]]) with len 16
#  then we don't need to define specific tensor for decreasing length of indexes
# Since the kronecker coefficient is 2, we need to define 2 random tensor in  otimes^3 CC^2.
# that is [[[random.randint(-5,5) for j in range(i)] for i in [n,n,n]] for s in range(r+1)] here n=2
# length of these tensors is not necessary to be big we can choose it to be r+1.
# in this case the indexes could be product(*[permutations(range(r+1), i) for i in [2,2,1,1]]) with length 324
# when I modify index and perturbations the problem was about the function has zero pattern because d= 6 and
# if we try to choose 4 number from range(6) two times then these two choice at least have two common numbers.
# so I modify the lower bound there to be three instead of two, in this case I got a basis with length
# 2 = kronecker coefficient as desired by us.
# since we dont know the tableau when we just get the tensor and estimated r , we need to find the tableau
# for this purpose I define a new function tableau and new fill new_kron

# if n= 3 then





from sage.all import *


def kron(lamb, mu, nu):
    Sym = SymmetricFunctions(QQ)
    s = Sym.schur()
    try:
        return s(lamb).itensor(s(mu)).monomial_coefficients()[Partition(nu)]
    except KeyError:
        return 0


def main():
    import sys
    n = int(sys.argv[1])
    tab = []
    k_mu = 0
    for i in range(6, 30):
        for j in Partitions(i, length = n).list():
            k = kron(j, j, j)
            if k > 2:
                tab = j
                k_mu = k
                break
        if k_mu > 2:
            break
    print [k_mu, tab]

main()

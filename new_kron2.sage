from sage.all import *


def kron(lamb, mu, nu):
    Sym = SymmetricFunctions(QQ)
    s = Sym.schur()
    try:
        return s(lamb).itensor(s(mu)).monomial_coefficients()[Partition(nu)]
    except KeyError:
        return 0


def main():
    A = flatten([cartesian_product([Partitions(d, length= i).list() for i in [3, 3, 3]]).list() for d in range(6, 10)])
    B = [[a, b, c, kron(a, b, c)] for a, b, c in A]
    for x in B:
        if not x[3] in [0, 1, 2]:
            print x


main()
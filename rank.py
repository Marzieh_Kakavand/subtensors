"""
Computes the rank of a matrix.

Depends on numpy.
"""

import sys
import numpy as np

def rank(A):
    try:
        M = np.matrix(eval(A))
        return np.linalg.matrix_rank(M)
    except:
        return 0

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: %s <A>"%sys.argv[0])
        print("Outputs the rank of A.")
        sys.exit(1)
    else:
        print(rank(sys.argv[1]))

from sage.all import *

def rank(A):
    try:
        M = Matrix(eval(A))
        return M.rank()
    except:
        return 0

if __name__ == '__main__':
    import sys
    matrix = sys.argv[1]
    print rank(matrix)

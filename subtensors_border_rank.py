from itertools import product, izip_longest, chain, starmap, permutations
import random
import sympy
import copy
import subprocess
from straighten_GL import straighten3h
import yaml


def sensible_tableau_and_its_kronecker_coeff(n):
    k = subprocess.check_output(["/Applications/SageMath/sage", "new_kron.sage", str(n)])
    return eval(k)


def tableau_transpose(tab):
    import operator
    res = all(starmap(operator.ge,zip(tab, tab[1:]))) & all(x >= 0 for x in tab)
    if not res:
        print 'The input should be a sequence of nonincreasing positive integers.'
    else:
        tab_range = map(lambda x: range(x), tab)
        trans_tab=[reduce(lambda x, y: x + y, tab_range).count(x) for x in range(tab[0])]
        return trans_tab


def random_perms(T):
    d = sum(T)
    t = range(d)
    random.shuffle(t)
    t1 = copy.deepcopy(t)
    random.shuffle(t)
    t2 = copy.deepcopy(t)
    return (t1, t2)


def matrix_rank(a):
    m_rank = subprocess.check_output(["python", "rank.py", str(a)])
    return int(m_rank)


def left_kernel_basis(a):
    kernel = subprocess.check_output(["/Applications/SageMath/sage", "left_kernel_basis.sage", str(a)])
    return eval(kernel)


def index_to_evaluate(r, tr_tab):
    perm_list = [list(permutations(range(r), i)) for i in tr_tab]
    return product(*perm_list)


def matrix_det(l):
    n = len(l)
    if n > 2:
        i = 1
        t = 0
        sum = 0
        while t <= n - 1:
            d = {}
            t1 = 1
            while t1 <= n - 1:
                m = 0
                d[t1] = []
                while m <= n - 1:
                    if m == t:
                        u = 0
                    else:
                        d[t1].append(l[t1][m])
                    m += 1
                t1 += 1
            l1 = [d[x] for x in d]
            sum = sum + i * (l[0][t]) * (matrix_det(l1))
            i = i * (-1)
            t += 1
        return sum
    else:
        if n == 2:
            return l[0][0] * l[1][1] - l[0][1] * l[1][0]
        else:
            return l[0][0]


def mode_det(mode, tensor_components, indices):
    return matrix_det([tensor_components[i][mode][0:len(indices)] for i in indices])


def evaluator(tau1, tau2, tensor_components, indexes, nu):
    s = 0
    for index in indexes:
        p = 1
        j = list(chain(*index))
        k = 0
        for n in nu:
            p = p * mode_det(0, tensor_components, [j[i]       for i in range(k, k + n)])
            p = p * mode_det(1, tensor_components, [j[tau1[i]] for i in range(k, k + n)])
            p = p * mode_det(2, tensor_components, [j[tau2[i]] for i in range(k, k + n)])
            k = k + n
        s = s + p
    return s


def construct_basis(r, n, k_mu, mu, nu):
    nu_permutations_of_tensors_lengths = list(product(*[list(permutations(range(r+1), w)) for w in nu]))
    max_tries = 30
    basis_matrix = []
    basis_rank = 0
    basis = []
    tensors = [[[[random.randint(-3, 3) for j in range(i)] for i in [n, n, n]] for s in range(r+1)] for w in range(k_mu)]
    perm = [random_perms(mu) for i in range(max_tries)]
    for tau1, tau2 in perm:
        new_matrix = copy.deepcopy(basis_matrix)
        row_completed = []
        for tensor in tensors:
            entry = evaluator(tau1, tau2, tensor, nu_permutations_of_tensors_lengths, nu)
            row_completed.append(entry)
        new_matrix.append(row_completed)
        new_rank = matrix_rank(new_matrix)
        if new_rank > basis_rank:
            basis_matrix = new_matrix
            basis_rank = new_rank
            basis.append([tau1,tau2])
            print 'basis_matrix: {}'.format(basis_matrix)
        if basis_rank == k_mu:
            break
    return basis


def left_kernel_coeffs(basis, r, n,  k_mu, nu):
    tensors = [[[[random.randint(-3, 3) for j in range(i)] for i in [n, n, n]] for s in range(r)] for w in range(k_mu)]
    nu_permutations_of_tensors_lengths = list(product(*[list(permutations(range(r), w)) for w in nu]))
    matrix = []
    for b1, b2 in basis:
        row_completed = []
        for tensor in tensors:
            entry = evaluator(b1, b2, tensor, nu_permutations_of_tensors_lengths, nu)
            row_completed.append(entry)
        matrix.append(row_completed)
    kernel_basis = left_kernel_basis(matrix)
    return kernel_basis


def evaluate_straighten(nu, tau1, tau2,  indexes):
    output = {}
    for index in indexes:
        j = list(chain(*index))
        k = 0
        tab = [[],]*3
        for n in nu:
            tab[0].append([j[i]       for i in range(k, k + n)])
            tab[1].append([j[tau1[i]] for i in range(k, k + n)])
            tab[2].append([j[tau2[i]] for i in range(k, k + n)])
            k = k + n
        st = straighten3h(tab[0], tab[1],tab[2])
        for k,v in st.iteritems():
            if v == 0:
                continue
            else:
                k_hash = int(''.join(map(str,sum(k[0] + k[1] + k[2], ()))))
                if k_hash in output:
                    w = output[k_hash] + v
                    if w == 0:
                        del output[k_hash]
                    else:
                        output[k_hash] = w
                else:
                    output[k_hash] = v
    return output


def verify_kernel_coeffs(r, nu, basis, kernel_coeffs):
    nu_permutations_of_tensors_length = product(*[list(permutations(range(r), i)) for i in nu])
    outputs_list = []
    j = 0
    for b1, b2 in basis:
        outputs = evaluate_straighten(nu, b1, b2, nu_permutations_of_tensors_length)
        outputs_list.append(outputs)
        j += 1
    basis_evals = []
    for i, outputs in enumerate(outputs_list):
        entry = {}
        for k, v in outputs.iteritems():
            if k in entry:
                x = entry[k] + v
                if x == 0:
                    del entry[k]
                else:
                    entry[k] = x
            else:
                entry[k] = v
        basis_evals.append(entry)

    for coeffs in kernel_coeffs:
        for i in range(len(basis_evals)):
            for k, v in basis_evals[i].iteritems():
                running_sum = coeffs[i] * v
                for j in range(i + 1, len(basis_evals)):
                    if k in basis_evals[j]:
                        running_sum += coeffs[j] * basis_evals[j][k]
                        del basis_evals[j][k]
                if not running_sum == 0:
                    print "kernel coefficients verification failed"
                    return False

    print "kernel coefficients verification succeeded"
    return True


def verify_kernel_coeffs_symbolically(r, n, nu, basis, kernel_coeffs):
    symbolic_tensor = [[[sympy.var("a%d%d%d" % (i, j, k)) for i in range(p)] for j, p in enumerate([n, n, n])] for k in range(r)]
    nu_permutations_of_tensors_length = list(product(*[list(permutations(range(r), w)) for w in nu]))

    basis_evals = []
    for b1, b2 in basis:
        outputs = evaluator(b1, b2, symbolic_tensor, nu_permutations_of_tensors_length, nu)
        basis_evals.append(outputs)

    for coeffs in kernel_coeffs:
        lin_comb = 0
        for i in range(len(basis_evals)):
            lin_comb = lin_comb + basis_evals[i] * coeffs[i]
        print sympy.expand(lin_comb)
        if not sympy.expand(lin_comb) == 0:
            print "symbolical verification failed"
            return False

    print "symbolical verification succeeded"
    return True


def evaluate_tensor(tensor, nu,  basis, coeffs_list):
    index = list(product(*[list(permutations(range(len(tensor)), w)) for w in nu]))
    basis_evals = []

    for b1,b2 in basis:
        outputs = evaluator(b1, b2, tensor, index, nu)
        basis_evals.append(outputs)

    for coeffs in coeffs_list:
        lin_comb = 0
        for i in range(len(basis_evals)):
            lin_comb += coeffs[i] * basis_evals[i]
        if not sympy.expand(lin_comb) == 0:
            print 'evaluation of this tensor is not zero'
            return True

    print "evaluation of this tensor is zero"
    return False


def main():
    yaml_doc = open('doc.yaml')
    parsed_yaml_doc = yaml.load(yaml_doc, Loader=yaml.FullLoader)
    tensor = eval(parsed_yaml_doc["tensor"])
    r = eval(parsed_yaml_doc["r"])
    #yaml_doc.close()

    n = len(tensor[0][0])
    [k_mu, mu] = sensible_tableau_and_its_kronecker_coeff(n)
    nu = tableau_transpose(mu)

    parsed_yaml_doc['n'] = str(n)
    yaml_doc_w = open('doc.yaml', 'w')
    yaml.dump(parsed_yaml_doc, yaml_doc_w)

    print 'tensor: {}'.format(tensor)
    print'r: {}'.format(r)
    print'n: {}'.format(n)
    print'mu: {}'.format(mu)
    print'k_mu: {}'.format(k_mu)
    print'nu: {}'.format(nu)

    while True:
        basis = construct_basis(r, n, k_mu, mu, nu)

        if len(basis) == k_mu:
            kernel_coeffs = left_kernel_coeffs(basis, r, n, k_mu, nu)

            if verify_kernel_coeffs(r, nu, basis, kernel_coeffs):
                print 'basis: {}'.format(basis)
                print 'kernel_coeffs: {}'.format(kernel_coeffs)

                parsed_yaml_doc['mu'] = str(mu)
                parsed_yaml_doc['k_mu'] = str(k_mu)
                parsed_yaml_doc['basis'] = str(basis)
                parsed_yaml_doc['kernel_coeffs'] = str(kernel_coeffs)
                yaml_doc_w = open('doc.yaml', 'w')
                yaml.dump(parsed_yaml_doc, yaml_doc_w)

                if verify_kernel_coeffs_symbolically(r, n,  nu, basis, kernel_coeffs):
                    evaluate_tensor(tensor, nu, basis, kernel_coeffs)
            break


main()


from itertools import product, chain, starmap, permutations
from straighten_GL import straighten3h
from functools import reduce
from scoop import futures
import subprocess
import datetime
import random
import sympy
import copy
import yaml
import json


def sensible_tableau_and_its_kronecker_coeff(n):
    k = subprocess.check_output(["/Applications/SageMath/sage", "new_kron.sage", str(n)])
    return eval(k)


def tableau_transpose(tab):
    import operator
    res = all(starmap(operator.ge,zip(tab, tab[1:]))) & all(x >= 0 for x in tab)
    if not res:
        print 'The input should be a sequence of nonincreasing positive integers.'
    else:
        tab_range = map(lambda x: range(x), tab)
        trans_tab = [reduce(lambda x, y: x + y, tab_range).count(x) for x in range(tab[0])]
        return trans_tab


def random_perms(T):
    d = sum(T[0])
    t = range(d)
    random.shuffle(t)
    t1 = copy.deepcopy(t)
    random.shuffle(t)
    t2 = copy.deepcopy(t)
    return (t1, t2)


def matrix_rank(a):
    m_rank = subprocess.check_output(["python", "rank.py", str(a)])
    return int(m_rank)


def left_kernel_basis(a):
    kernel = subprocess.check_output(["/Applications/SageMath/sage", "left_kernel_basis.sage", str(a)])
    return eval(kernel)


def matrix_det(l):
    n = len(l)
    if n > 2:
        i = 1
        t = 0
        sum = 0
        while t <= n - 1:
            d = {}
            t1 = 1
            while t1 <= n - 1:
                m = 0
                d[t1] = []
                while m <= n - 1:
                    if m == t:
                        u = 0
                    else:
                        d[t1].append(l[t1][m])
                    m += 1
                t1 += 1
            l1 = [d[x] for x in d]
            sum = sum + i * (l[0][t]) * (matrix_det(l1))
            i = i * (-1)
            t += 1
        return sum
    else:
        if n == 2:
            return l[0][0] * l[1][1] - l[0][1] * l[1][0]
        else:
            return l[0][0]


def mode_det(mode, tensor_components, indices):
    return matrix_det([tensor_components[i][mode][0:len(indices)] for i in indices])


def evaluate(tau1, tau2, tensor_components, indexes, nu):
    s = 0
    for j in indexes:
        p = 1
        k = 0
        for n in nu[0]:
            p = p * mode_det(0, tensor_components, [j[i]       for i in range(k, k + n)])
            k = k + n
        k = 0
        for n in nu[1]:
            p = p * mode_det(1, tensor_components, [j[tau1[i]] for i in range(k, k + n)])
            k = k + n
        k = 0
        for n in nu[2]:
            p = p * mode_det(2, tensor_components, [j[tau2[i]] for i in range(k, k + n)])
            k = k + n
        s = s + p
    return s


def nu_permutations_of_tensors_lengths(dic, nu):
    minlen = min(len(nu[0]), len(nu[1]), len(nu[2]))
    j = 0
    for i in range(minlen):
        if nu[0][i] == nu[1][i] == nu[2][i]:
            j = j + 1
        else:
            break
    same_indices = [list(chain(*x)) for x in list(product(*[dic[s] for s in nu[0][0: j]]))]
    i_0 = set([str(list(chain(*x))) for x in list(product(*[dic[s] for s in nu[0][j:]]))])
    i_1 = set([str(list(chain(*x))) for x in list(product(*[dic[s] for s in nu[1][j:]]))])
    i_2 = set([str(list(chain(*x))) for x in list(product(*[dic[s] for s in nu[2][j:]]))])
    different_indices = map(json.loads, i_0.intersection(i_1, i_2))
    output = [list(chain(*x)) for x in list(product(*[same_indices, different_indices]))]
    return output


def construct_basis(w, nu_permutation_w, n, k_mu, mu, nu, max_tries, mod):
    basis_matrix = []
    basis_rank = 0
    basis = []

    tensors = [[[[random.randint(-3, 3) for j in range(i)] for i in n] for s in range(w)] for t in range(k_mu)]

    perm = [random_perms(mu) for i in range(max_tries)]

    for tau1, tau2 in perm:
        new_matrix = copy.deepcopy(basis_matrix)
        row_completed = []
        for tensor in tensors:
            a = len(nu_permutation_w)
            partitions = [nu_permutation_w[i * mod: min(((i + 1) * mod) - 1, a)] for i in range((a + mod - 1) / mod)]

            def evalate_part(x):
                return evaluate(tau1, tau2, tensor, x, nu)

            outputs = futures.map(evalate_part, partitions)
            entry = sum(outputs)
            row_completed.append(entry)
        new_matrix.append(row_completed)
        new_rank = matrix_rank(new_matrix)
        #print 'new_matrix: {}'.format(new_matrix)
        #print 'new_rank: {}'.format(new_rank)
        if new_rank > basis_rank:
            basis_matrix = new_matrix
            basis_rank = new_rank
            basis.append([tau1,tau2])
        if basis_rank == k_mu:
            break
    return basis


def left_kernel_coeffs(basis, r, nu_permutation_r,  n,  k_mu, nu):
    tensors = [[[[random.randint(-3, 3) for j in range(i)] for i in n] for s in range(r)] for w in range(k_mu)]
    matrix = []
    for b1, b2 in basis:
        row_completed = []
        for tensor in tensors:
            entry = evaluate(b1, b2, tensor, nu_permutation_r, nu)
            row_completed.append(entry)
        matrix.append(row_completed)
    print 'matrix'
    print matrix
    kernel_basis = left_kernel_basis(matrix)
    return kernel_basis


def evaluate_straighten(nu, tau1, tau2,  indexes):
    output = {}
    for j in indexes:
        tab_0 = []
        tab_1 = []
        tab_2 = []
        k = 0
        for n in nu[0]:
            tab_0.append([j[i]       for i in range(k, k + n)])
            k = k + n
        k = 0
        for n in nu[1]:
            tab_1.append([j[tau1[i]] for i in range(k, k + n)])
            k = k + n
        k = 0
        for n in nu[2]:
            tab_2.append([j[tau2[i]] for i in range(k, k + n)])
            k = k + n
        st = straighten3h(tab_0, tab_1, tab_2)

        for k,v in st.iteritems():
            if v == 0:
                continue
            else:
                k_hash = int(''.join(map(str,sum(k[0] + k[1] + k[2], ()))))
                if k_hash in output:
                    w = output[k_hash] + v
                    if w == 0:
                        del output[k_hash]
                    else:
                        output[k_hash] = w
                else:
                    output[k_hash] = v
    return output


def verify_kernel_coeffs(r, nu_permutation_r, nu, basis, kernel_coeffs,  mod):
    outputs_list = []
    for b1, b2 in basis:
        a = len(nu_permutation_r)
        partitions = [nu_permutation_r[i * mod: min(((i + 1) * mod) - 1, a)] for i in range((a + mod - 1) / mod)]

        def evaluate_straighten_part(part):
            return evaluate_straighten(nu, b1, b2, part)

        outputs = futures.map(evaluate_straighten_part, partitions)
        outputs_list.append(outputs)

    basis_evals = []
    for outputs in outputs_list:
        entry = {}
        for output in outputs:
            for k, v in output.iteritems():
                if k in entry:
                    x = entry[k] + v
                    if x == 0:
                        del entry[k]
                    else:
                        entry[k] = x
                else:
                    entry[k] = v
        basis_evals.append(entry)

    for coeffs in kernel_coeffs:
        for i in range(len(basis_evals)):
            for k, v in basis_evals[i].iteritems():
                running_sum = coeffs[i] * v
                for j in range(i + 1, len(basis_evals)):
                    if k in basis_evals[j]:
                        running_sum += coeffs[j] * basis_evals[j][k]
                        del basis_evals[j][k]
                if not running_sum == 0:
                    print "kernel coefficients verification failed"
                    return False

    print "kernel coefficients verification succeeded"
    return True


def verify_kernel_coeffs_symbolically(r, nu_permutation_r, n, nu, basis, kernel_coeffs, mod):
    symbolic_tensor = [[[sympy.var("a%d%d%d" % (i, j, k)) for i in range(p)] for j, p in enumerate(n)] for k in
                       range(r - 1)] + [[[1] + [0, ] * (p - 1) for p in n]]


    basis_evals = []

    for b1, b2 in basis:
        a = len(nu_permutation_r)
        partitions = [nu_permutation_r[i * mod: min(((i + 1) * mod) - 1, a)] for i in
                      range((a + mod - 1) / mod)]

        def evalate_part(part):
            return evaluate(b1, b2, symbolic_tensor, part, nu)

        outputs = futures.map(evalate_part, partitions)
        entry = sum(outputs)
        basis_evals.append(entry)

    for coeffs in kernel_coeffs:
        lin_comb = 0
        for i in range(len(basis_evals)):
            lin_comb = lin_comb + basis_evals[i] * coeffs[i]
        print sympy.expand(lin_comb)
        if not sympy.expand(lin_comb) == 0:
            print "symbolical verification failed"
            return False

    print "symbolical verification succeeded"
    return True


def evaluate_tensor(tensors, nu, nu_permutation_w,  basis, coeffs_list, mod):
    for tensor in tensors:
        print tensor
        a = len(nu_permutation_w)
        partitions = [nu_permutation_w[i * mod: min(((i + 1) * mod) - 1, a)] for i in range((a + mod - 1) / mod)]

        basis_evals = []
        for b1, b2 in basis:
            def evalate_part(x):
                return evaluate(b1, b2, tensor, x, nu)
            outputs = futures.map(evalate_part, partitions)
            entry = sum(outputs)
            basis_evals.append(entry)

        print 'basis_evals{}'.format(basis_evals)
        print 'coeffs_list:{}'.format(coeffs_list)
        for coeffs in coeffs_list:
            lin_comb = 0
            for i in range(len(basis_evals)):
                print 'coeffs[i]:{}'.format(coeffs[i])
                print 'basis_evals[i]{}'.format(basis_evals[i])
                lin_comb += coeffs[i] * basis_evals[i]
            if not sympy.expand(lin_comb) == 0:
                print'coeffs'
                print coeffs
                print 'evaluation of this tensor is not zero'
                return True

    return False


def main():
    starts_at = datetime.datetime.now()
    print 'This code starts to run at: {}'.format(starts_at)
    yaml_doc = open('doc.yaml')
    yaml_doc = yaml.load(yaml_doc, Loader=yaml.FullLoader)
    tensors = eval(yaml_doc['tensors'])
    r = eval(yaml_doc['r'])
    yaml_doc_w = open('doc.yaml', 'w')
    yaml.dump(yaml_doc, yaml_doc_w)

    w = len(tensors)
    n = [len(tensors[0][0][0]), len(tensors[0][0][1]), len(tensors[0][0][2])]
    k_mu = 3
    mu = [[9, 5], [9, 5], [10, 4]]
    nu = [tableau_transpose(mu[0]), tableau_transpose(mu[1]),tableau_transpose(mu[2])]

    a = list(set(list(chain(*nu))))
    required_permutations_of_w = dict([(i, list(permutations(range(w), i))) for i in a])
    required_permutations_of_r = dict([(i, list(permutations(range(r), i))) for i in a])
    nu_permutation_w = nu_permutations_of_tensors_lengths(required_permutations_of_w, nu)
    nu_permutation_r = nu_permutations_of_tensors_lengths(required_permutations_of_r, nu)

    indices_computed_at = datetime.datetime.now()
    permuted_indices_computing_duration = indices_computed_at-starts_at
    indices_term_days = permuted_indices_computing_duration.days
    indices_term_hours = divmod(permuted_indices_computing_duration.seconds, 3600)
    duration_minutes = divmod(indices_term_hours[1], 60)
    print 'Computing the permuted indices takes {} days, {} hours and {} minutes.'.format(indices_term_days,
                                                                                         indices_term_hours[0],
                                                                                         duration_minutes[0])

    yaml_docs = open('documents2.yaml', 'r')
    yaml_documents = yaml.load(yaml_docs, Loader=yaml.FullLoader) or {}
    yaml_documents.update({str(starts_at): {}})
    yaml_documents[str(starts_at)].update({
        'tensor': tensors,
        'r': r,
        'w': w,
        'nu': nu,
        'n': n,
        'mu': mu,
        'kronecker_coeff': k_mu,
        'Computing the permuted indices takes': '{} days, {} hours and {} minutes'.format(indices_term_days,
                                                                                          indices_term_hours[0],
                                                                                          duration_minutes[0])})
    yaml_documents_w = open('documents2.yaml', 'w')
    yaml.dump(yaml_documents, yaml_documents_w)


    mod = 200
    max_tries = 20 * k_mu
    while True:
        basis = construct_basis(w, nu_permutation_w, n, k_mu, mu, nu, max_tries, mod)

        if len(basis) == k_mu:
            print 'basis: {}'.format(basis)
            basis_computed_at = datetime.datetime.now()
            basis_computing_duration = basis_computed_at - indices_computed_at
            basis_terms_days = basis_computing_duration.days
            basis_terms_hours = divmod(basis_computing_duration.seconds, 3600)
            basis_terms_minutes = divmod(basis_terms_hours[1], 60)
            print 'Computing the basis takes {} days, {} hours and {} minutes.'.format(basis_terms_days,
                                                                                      basis_terms_hours[0],
                                                                                      basis_terms_minutes[0])
            kernel_coeffs = left_kernel_coeffs(basis, r, nu_permutation_r, n, k_mu, nu)

            kernel_coeffs_computed_at = datetime.datetime.now()
            kernel_coeffs_computing_duration = kernel_coeffs_computed_at-basis_computed_at
            kernel_coeffs_terms_days = kernel_coeffs_computing_duration.days
            kernel_coeffs_terms_hours = divmod(kernel_coeffs_computing_duration.seconds, 3600)
            kernel_coeffs_terms_minutes = divmod(kernel_coeffs_terms_hours[1], 60)
            print 'Computing the kernel_coeffs takes {} days, {} hours and {} minutes.'.format(
                                                                                    kernel_coeffs_terms_days,
                                                                                    kernel_coeffs_terms_hours[0],
                                                                                    kernel_coeffs_terms_minutes[0])


            sym_verification = verify_kernel_coeffs_symbolically(r, nu_permutation_r, n, nu, basis, kernel_coeffs, mod)

            sym_verification_computed_at = datetime.datetime.now()
            sym_verification_computing_duration = sym_verification_computed_at - kernel_coeffs_computed_at
            sym_verification_terms_days = sym_verification_computing_duration.days
            sym_verification_terms_hours = divmod(sym_verification_computing_duration.seconds, 3600)
            sym_verification_terms_minutes = divmod(sym_verification_terms_hours[1], 60)
            print 'Computing the symbolically_verification takes {} days, {} hours and {} minutes.'.format(
                                                                                    sym_verification_terms_days,
                                                                                    sym_verification_terms_hours[0],
                                                                                    sym_verification_terms_minutes[0])


            yaml_docs = open('documents2.yaml', 'r')
            yaml_documents = yaml.load(yaml_docs, Loader=yaml.FullLoader)
            yaml_documents[str(starts_at)].update({
                'basis': basis,
                'kernel_coeffs': kernel_coeffs,
                'Computing the basis takes': '{} days, {} hours and {} minutes'.format(
                                                                                    basis_terms_days,
                                                                                    basis_terms_hours[0],
                                                                                    basis_terms_minutes[0]),
                'Computing the kernel_coeffs takes': '{} days, {} hours and {} minutes'.format(
                                                                                    kernel_coeffs_terms_days,
                                                                                    kernel_coeffs_terms_hours[0],
                                                                                    kernel_coeffs_terms_minutes[0]),
                'Computing the symbolically_verification takes': '{} days, {} hours and {} minutes'.format(
                                                                                    sym_verification_terms_days,
                                                                                    sym_verification_terms_hours[0],
                                                                                    sym_verification_terms_minutes[0])})
            yaml_documents_w = open('documents2.yaml', 'w')
            yaml.dump(yaml_documents, yaml_documents_w)

            #if verify_kernel_coeffs(r, dicr, nu, basis, kernel_coeffs, mod):
            if sym_verification:
                result = evaluate_tensor(tensors, nu, nu_permutation_w, basis, kernel_coeffs, mod)

                tensor_evaluate_at = datetime.datetime.now()
                tensor_evaluating_duration = tensor_evaluate_at - kernel_coeffs_computed_at
                tensor_evaluating_terms_days = tensor_evaluating_duration.days
                tensor_evaluating_terms_hours = divmod(tensor_evaluating_duration.seconds, 3600)
                tensor_evaluating_terms_minutes = divmod(tensor_evaluating_terms_hours[1], 60)
                print 'Evaluating the tensor takes {} days, {} hours and {} minutes'.format(
                                                                                    tensor_evaluating_terms_days,
                                                                                    tensor_evaluating_terms_hours[0],
                                                                                    tensor_evaluating_terms_minutes[0])

                yaml_docs = open('documents2.yaml', 'r')
                yaml_documents = yaml.load(yaml_docs, Loader=yaml.FullLoader)
                yaml_documents[str(starts_at)].update({
                    'result': result,
                    'Evaluating the tensor takes': '{} days, {} hours and {} minutes.'.format(
                                                                                    tensor_evaluating_terms_days,
                                                                                    tensor_evaluating_terms_hours[0],
                                                                                    tensor_evaluating_terms_minutes[0])
})
                yaml_documents_w = open('documents2.yaml', 'w')
                yaml.dump(yaml_documents, yaml_documents_w)

            total_duration = tensor_evaluate_at - starts_at
            total_terms_days = total_duration.days
            total_terms_hours = divmod(total_duration.seconds, 3600)
            total_terms_minutes = divmod(total_terms_hours[1], 60)
            print 'Running this code takes {} days, {} hours and {} minutes.'.format(total_terms_days,
                                                                                     total_terms_hours[0],
                                                                                     total_terms_minutes[0])
            break


main()
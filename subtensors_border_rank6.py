from itertools import product, chain, starmap, permutations
from straighten_GL import straighten3h
from functools import reduce
from scoop import futures
import subprocess
import datetime
import random
import sympy
import copy
import yaml
import json


def sensible_tableau_and_its_kronecker_coeff(n):
    k = subprocess.check_output(["/Applications/SageMath/sage", "new_kron.sage", str(n)])
    return eval(k)


def tableau_transpose(tab):
    import operator
    res = all(starmap(operator.ge,zip(tab, tab[1:]))) & all(x >= 0 for x in tab)
    if not res:
        print 'The input should be a sequence of nonincreasing positive integers.'
    else:
        tab_range = map(lambda x: range(x), tab)
        trans_tab = [reduce(lambda x, y: x + y, tab_range).count(x) for x in range(tab[0])]
        return trans_tab


def random_perms(T):
    d = sum(T[0])
    t = range(d)
    random.shuffle(t)
    t1 = copy.deepcopy(t)
    random.shuffle(t)
    t2 = copy.deepcopy(t)
    return (t1, t2)


def matrix_rank(a):
    m_rank = subprocess.check_output(["python", "rank.py", str(a)])
    return int(m_rank)


def left_kernel_basis(a):
    kernel = subprocess.check_output(["/Applications/SageMath/sage", "left_kernel_basis.sage", str(a)])
    return eval(kernel)


def matrix_det(l):
    n = len(l)
    if n > 2:
        i = 1
        t = 0
        sum = 0
        while t <= n - 1:
            d = {}
            t1 = 1
            while t1 <= n - 1:
                m = 0
                d[t1] = []
                while m <= n - 1:
                    if m == t:
                        u = 0
                    else:
                        d[t1].append(l[t1][m])
                    m += 1
                t1 += 1
            l1 = [d[x] for x in d]
            sum = sum + i * (l[0][t]) * (matrix_det(l1))
            i = i * (-1)
            t += 1
        return sum
    else:
        if n == 2:
            return l[0][0] * l[1][1] - l[0][1] * l[1][0]
        else:
            return l[0][0]


def mode_det(mode, tensor_components, indices):
    return matrix_det([tensor_components[i][mode][0:len(indices)] for i in indices])


def evaluate(tau1, tau2, tensor_components, indexes, nu):
    s = 0
    for j in indexes:
        p = 1
        k = 0
        for n in nu[0]:
            p = p * mode_det(0, tensor_components, [j[i]       for i in range(k, k + n)])
            k = k + n
        k = 0
        for n in nu[1]:
            p = p * mode_det(1, tensor_components, [j[tau1[i]] for i in range(k, k + n)])
            k = k + n
        k = 0
        for n in nu[2]:
            p = p * mode_det(2, tensor_components, [j[tau2[i]] for i in range(k, k + n)])
            k = k + n
        s = s + p
    return s


def nu_permutations_of_tensors_lengths(dic, nu):
    minlen = min(len(nu[0]), len(nu[1]), len(nu[2]))
    j = 0
    for i in range(minlen):
        if nu[0][i] == nu[1][i] == nu[2][i]:
            j = j + 1
        else:
            break
    same_indices = [list(chain(*x)) for x in list(product(*[dic[s] for s in nu[0][0: j]]))]
    i_0 = set([str(list(chain(*x))) for x in list(product(*[dic[s] for s in nu[0][j:]]))])
    i_1 = set([str(list(chain(*x))) for x in list(product(*[dic[s] for s in nu[1][j:]]))])
    i_2 = set([str(list(chain(*x))) for x in list(product(*[dic[s] for s in nu[2][j:]]))])
    different_indices = map(json.loads, i_0.intersection(i_1, i_2))
    output = [list(chain(*x)) for x in list(product(*[same_indices, different_indices]))]
    return output


def construct_basis(w, nu_permutation_w, n, k_mu, mu, nu, max_tries, mod):
    basis_matrix = []
    basis_rank = 0
    basis = []

    tensors = [[[[random.randint(-3, 3) for j in range(i)] for i in n] for s in range(w)] for t in range(k_mu)]

    perm = [random_perms(mu) for i in range(max_tries)]

    for tau1, tau2 in perm:
        new_matrix = copy.deepcopy(basis_matrix)
        row_completed = []
        for tensor in tensors:
            a = len(nu_permutation_w)
            partitions = [nu_permutation_w[i * mod: min(((i + 1) * mod) - 1, a)] for i in range((a + mod - 1) / mod)]

            def evalate_part(x):
                return evaluate(tau1, tau2, tensor, x, nu)

            outputs = futures.map(evalate_part, partitions)
            entry = sum(outputs)
            row_completed.append(entry)
        new_matrix.append(row_completed)
        new_rank = matrix_rank(new_matrix)
        #print 'new_matrix: {}'.format(new_matrix)
        #print 'new_rank: {}'.format(new_rank)
        if new_rank > basis_rank:
            basis_matrix = new_matrix
            basis_rank = new_rank
            basis.append([tau1,tau2])
        if basis_rank == k_mu:
            break
    return basis


def left_kernel_coeffs(basis, r, nu_permutation_r,  n,  k_mu, nu):
    tensors = [[[[random.randint(-3, 3) for j in range(i)] for i in n] for s in range(r)] for w in range(k_mu)]
    matrix = []
    for b1, b2 in basis:
        row_completed = []
        for tensor in tensors:
            entry = evaluate(b1, b2, tensor, nu_permutation_r, nu)
            row_completed.append(entry)
        matrix.append(row_completed)
    print 'matrix'
    print matrix
    kernel_basis = left_kernel_basis(matrix)
    return kernel_basis


def evaluate_straighten(nu, tau1, tau2,  indexes):
    output = {}
    for j in indexes:
        tab_0 = []
        tab_1 = []
        tab_2 = []
        k = 0
        for n in nu[0]:
            tab_0.append([j[i]       for i in range(k, k + n)])
            k = k + n
        k = 0
        for n in nu[1]:
            tab_1.append([j[tau1[i]] for i in range(k, k + n)])
            k = k + n
        k = 0
        for n in nu[2]:
            tab_2.append([j[tau2[i]] for i in range(k, k + n)])
            k = k + n
        st = straighten3h(tab_0, tab_1, tab_2)

        for k,v in st.iteritems():
            if v == 0:
                continue
            else:
                k_hash = int(''.join(map(str,sum(k[0] + k[1] + k[2], ()))))
                if k_hash in output:
                    w = output[k_hash] + v
                    if w == 0:
                        del output[k_hash]
                    else:
                        output[k_hash] = w
                else:
                    output[k_hash] = v
    return output


def verify_kernel_coeffs(r, nu_permutation_r, nu, basis, kernel_coeffs,  mod):
    outputs_list = []
    for b1, b2 in basis:
        a = len(nu_permutation_r)
        partitions = [nu_permutation_r[i * mod: min(((i + 1) * mod) - 1, a)] for i in range((a + mod - 1) / mod)]

        def evaluate_straighten_part(part):
            return evaluate_straighten(nu, b1, b2, part)

        outputs = futures.map(evaluate_straighten_part, partitions)
        outputs_list.append(outputs)

    basis_evals = []
    for outputs in outputs_list:
        entry = {}
        for output in outputs:
            for k, v in output.iteritems():
                if k in entry:
                    x = entry[k] + v
                    if x == 0:
                        del entry[k]
                    else:
                        entry[k] = x
                else:
                    entry[k] = v
        basis_evals.append(entry)

    for coeffs in kernel_coeffs:
        for i in range(len(basis_evals)):
            for k, v in basis_evals[i].iteritems():
                running_sum = coeffs[i] * v
                for j in range(i + 1, len(basis_evals)):
                    if k in basis_evals[j]:
                        running_sum += coeffs[j] * basis_evals[j][k]
                        del basis_evals[j][k]
                if not running_sum == 0:
                    print "kernel coefficients verification failed"
                    return False

    print "kernel coefficients verification succeeded"
    return True


def verify_kernel_coeffs_symbolically(r, nu_permutation_r, n, nu, basis, kernel_coeffs, mod):
    symbolic_tensor = [[[sympy.var("a%d%d%d" % (i, j, k)) for i in range(p)] for j, p in enumerate(n)] for k in
                       range(r - 1)] + [[[1] + [0, ] * (p - 1) for p in n]]


    basis_evals = []

    for b1, b2 in basis:
        a = len(nu_permutation_r)
        partitions = [nu_permutation_r[i * mod: min(((i + 1) * mod) - 1, a)] for i in
                      range((a + mod - 1) / mod)]

        def evalate_part(part):
            return evaluate(b1, b2, symbolic_tensor, part, nu)

        outputs = futures.map(evalate_part, partitions)
        entry = sum(outputs)
        basis_evals.append(entry)

    for coeffs in kernel_coeffs:
        lin_comb = 0
        for i in range(len(basis_evals)):
            lin_comb = lin_comb + basis_evals[i] * coeffs[i]
        print sympy.expand(lin_comb)
        if not sympy.expand(lin_comb) == 0:
            print "symbolical verification failed"
            return False

    print "symbolical verification succeeded"
    return True


def evaluate_tensor(tensors, nu, nu_permutation_w,  basis, coeffs_list, mod):
    for tensor in tensors:
        print tensor
        a = len(nu_permutation_w)
        partitions = [nu_permutation_w[i * mod: min(((i + 1) * mod) - 1, a)] for i in range((a + mod - 1) / mod)]

        basis_evals = []
        for b1, b2 in basis:
            def evalate_part(x):
                return evaluate(b1, b2, tensor, x, nu)
            outputs = futures.map(evalate_part, partitions)
            entry = sum(outputs)
            basis_evals.append(entry)

        for coeffs in coeffs_list:
            lin_comb = 0
            for i in range(len(basis_evals)):
                print 'coeffs[i]:{}'.format(coeffs[i])
                print 'basis_evals[i]{}'.format(basis_evals[i])
                lin_comb += coeffs[i] * basis_evals[i]
            if not sympy.expand(lin_comb) == 0:
                print'coeffs'
                print coeffs
                print 'evaluation of this tensor is not zero'
                return True
    return False


def time_expended(starting_time, deed):
    computed_at = datetime.datetime.now()
    computing_duration = computed_at-starting_time
    days = computing_duration.days
    hours = divmod(computing_duration.seconds, 3600)
    minutes = divmod(hours[1], 60)
    print 'Computing {} takes {} days, {} hours and {} minutes.'.format(deed, days, hours[0], minutes[0])
    term = {'Computing {} takes'.format(deed): '{} days, {} hours and {} minutes'.format(deed, days, hours[0], minutes[0])}
    return [term, computed_at, days, hours[0], minutes[0]]


def saving_documents_yaml(file_name, dict_name,  dict_document):
    yaml_docs = open(file_name, 'r')
    yaml_documents = yaml.load(yaml_docs, Loader=yaml.FullLoader) or {}
    yaml_documents.update({dict_name: {}})
    yaml_documents[dict_name].update(dict_document)
    yaml_documents_w = open(file_name, 'w')
    yaml.dump(yaml_documents, yaml_documents_w)


def main():
    starts_at = datetime.datetime.now()
    print 'This code starts to run at: {}'.format(starts_at)
    yaml_doc = open('doc.yaml')
    yaml_doc = yaml.load(yaml_doc, Loader=yaml.FullLoader)
    tensors = eval(yaml_doc['tensors'])
    r = eval(yaml_doc['r'])
    yaml_doc_w = open('doc.yaml', 'w')
    yaml.dump(yaml_doc, yaml_doc_w)

    w = len(tensors[0])
    n = [len(tensors[0][0][0]), len(tensors[0][0][1]), len(tensors[0][0][2])]
    k_mu = 4
    mu = [[12, 6], [12, 6], [12, 6]]
    nu = [tableau_transpose(mu[0]), tableau_transpose(mu[1]),tableau_transpose(mu[2])]
    saving_documents_yaml('documents2.yaml', str(starts_at), {'tensors': tensors,
                                                              'r': r,
                                                              'w': w,
                                                              'nu': nu,
                                                              'n': n,
                                                              'mu': mu,
                                                              'kronecker_coeff': k_mu})
    a = list(set(list(chain(*nu))))
    required_permutations_of_w = dict([(i, list(permutations(range(w), i))) for i in a])
    required_permutations_of_r = dict([(i, list(permutations(range(r), i))) for i in a])
    nu_permutation_w = nu_permutations_of_tensors_lengths(required_permutations_of_w, nu)
    nu_permutation_r = nu_permutations_of_tensors_lengths(required_permutations_of_r, nu)

    indices_computing_term = time_expended(starts_at, 'permuted indices')
    saving_documents_yaml('documents2.yaml',str(starts_at), indices_computing_term[0])

    mod = 200
    max_tries = 20 * k_mu
    while True:
        basis = construct_basis(w, nu_permutation_w, n, k_mu, mu, nu, max_tries, mod)

        if len(basis) == k_mu:
            print 'basis: {}'.format(basis)
            saving_documents_yaml('documents2.yaml', str(starts_at), {'basis': basis})
            basis_computing_term = time_expended(indices_computing_term[1], 'basis')
            saving_documents_yaml('documents2.yaml', str(starts_at), basis_computing_term[0])

            kernel_coeffs = left_kernel_coeffs(basis, r, nu_permutation_r, n, k_mu, nu)
            saving_documents_yaml('documents2.yaml', str(starts_at), {'kernel_coeffs': kernel_coeffs})
            kernel_coeffs_computing_term = time_expended(basis_computing_term[1], 'kernel_coeffs')
            saving_documents_yaml('documents2.yaml', str(starts_at), kernel_coeffs_computing_term[0])

            sym_verification = verify_kernel_coeffs_symbolically(r, nu_permutation_r, n, nu, basis, kernel_coeffs, mod)
            saving_documents_yaml('documents2.yaml', str(starts_at), {'sym_verification': sym_verification})
            sym_verification_computing_term = time_expended(kernel_coeffs_computing_term[1], 'symbolically verification')
            saving_documents_yaml('documents2.yaml', str(starts_at), sym_verification_computing_term[0])

            #if verify_kernel_coeffs(r, dicr, nu, basis, kernel_coeffs, mod):
            if sym_verification:
                results = evaluate_tensor(tensors, nu, nu_permutation_w, basis, kernel_coeffs, mod)
                saving_documents_yaml('documents2.yaml', str(starts_at),{'results': results})
                tensors_computing_term = time_expended(sym_verification_computing_term[1], 'tensors')
                saving_documents_yaml('documents2.yaml', str(starts_at), tensors_computing_term[0])

            total_computing_term = time_expended(starts_at, 'in total')
            saving_documents_yaml('documents2.yaml', str(starts_at), total_computing_term[0])
            break


main()
